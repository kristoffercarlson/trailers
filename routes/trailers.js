var express        = require('express');
var router         = express.Router();
var url            = require('url');
var trailerService = require('../lib/trailerService')

/**
 * Get trailers.
 */
router.get('/', function(req, res) {
  trailerService.getTrailerUrls (

      // Retrieve the 'resource' query parameter
      url.parse(req.url, true).query['resource'],

      // On success
      function(data) {
        res.writeHead(200, 'OK');
        res.write( JSON.stringify(data) );
        res.end();
      },
      // On error

      function(err) {
          res.writeHead(500, 'INTERNAL SERVER ERROR');
          res.write( JSON.stringify(err) );
          res.end();
      }
  );
});

module.exports = router;
