var expect         = require('chai').expect;
var trailerService = require('../lib/trailerService');

describe('trailerService', function() {
    it('throw an exception with a bad url', function() {
        expect(function() {
            trailerService.getTrailerUrls();
        })
        .to.throw('Bad resource url');
    });
    it('throw an exception with an invalid url', function() {
        expect(function() {
            trailerService.getTrailerUrls('http.//www,this.c///');
        })
        .to.throw('Bad resource url');
    });

    it('should not throw an exception with a good url', function() {
        expect(function() {
            trailerService.getTrailerUrls('https://content.viaplay.se/web-se/film/the-counselor-2013?partial=true&block=1');
        })
        .to.not.throw('Bad resource url');
    });
});