var expect         = require('chai').expect;
var http           = require('http');
var app            = require('../app');
var trailerService = require('../lib/trailerService');

var server;

describe('trailerService', function() {
    before(function () {
        server = app.listen(3001, function () {
            var host = server.address().address;
            var port = server.address().port;

            console.log('Example app listening at http://%s:%s', host, port);
        });
    });

    after(function () {
        server.close();
    });

    it('returns 500 to an improper GET request', function(done) {
        http.get('http://localhost:3001/trailers', function (res) {
            expect(res.statusCode).to.equal(500);
            done();
        });
    });

    it('returns 200 to a proper GET request', function(done) {
        http.get('http://localhost:3001/trailers?resource=https%3A//content.viaplay.se/web-se/film/the-counselor-2013%3Fpartial%3Dtrue%26block%3D1', function (res) {
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
});