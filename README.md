How do we build and run it?
==========================
- Build: n/a, but install packates with npm install
- Run with: npm start (To enable caching, a local Redis server should be up and running on the standard port.)
- To use the service:  http://localhost:3000/trailers?resource=
- The resource is a url encoded viaplay movie resource link.
- e.g. http://localhost:3000/trailers?resource=https%3A//content.viaplay.se/web-se/film/the-counselor-2013%3Fpartial%3Dtrue%26block%3D1
- Test with: npm test

What tools did you use?
=======================
- IntelliJ IDEA generated skeleton
- Express for serving REST requests
- mocha and chai for testing

Why did you use them?
=====================
- To keep it simple

Did you intentionally leave stuff out?
======================================
- Yes

In that case, what and why?
===========================
- Loads of testing, more testable code.
- Left out due to time restraints
