var apiRequest = require('./request');
var Promise    = require('promise');

var viaPlayApi = {
    /**
     *
     * @param viaPlayMovieResource from _embedded["viaplay:blocks"][0]._embedded["viaplay:products"][1]._links.self.href
     * loaded from @http://content.viaplay.se/web-se/film
     * @returns {Promise}
     */
    getImdbId: function(viaPlayMovieResource) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            apiRequest.request({
                url      : viaPlayMovieResource,
                callback : function(err, res) {
                    if (err) reject(err);
                    else     resolve( _this._imdbId(res, reject) );
                }
            });
        });
    },

    _imdbId: function(data, reject) {
        try {
            return JSON.parse(data)
                //._embedded["viaplay:blocks"][0]
                ._embedded['viaplay:product']
                .content.imdb.id.substring(2);
        }
        catch(err) {
            reject(err.message);
        }

    }

};

module.exports = viaPlayApi;