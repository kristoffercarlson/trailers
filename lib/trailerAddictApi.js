var apiRequest  = require('./request');
var parseString = require('xml2js').parseString;
var endpoint         = 'http://api.traileraddict.com/?imdb=',
    trailerAddictApi = {
        /**
         *
         * @param imdbId
         */
        getTrailers: function(imdbId) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                apiRequest.request({
                    url      : endpoint + imdbId,
                    callback : function(err, res) {
                        if (err) reject(err);
                        else {
                            parseString(res, function(err, data) {
                                if (err) reject(err);
                                else     resolve( _this._getLink(data, reject) );
                            });
                        }
                    }
                });
            });
        },
        _getLink: function(data, reject) {
            try {
                return data.trailers.trailer[0].link[0];
            }
            catch(err) {
                reject(err.message);
            }
        }
    };

module.exports = trailerAddictApi;