var validUrl          = require('valid-url');
var viaPlayApi        = require('./viaPlayApi');
var trailerAddictApi  = require('./trailerAddictApi');
var Optional          = require('./optional');
var redis             = require("redis"),
    client            = redis.createClient(),
    cacheDisabled;

client.on("error", function (err) {
    cacheDisabled = err.code == 'ECONNREFUSED';
    console.log(err);
});

client.on("ready", function () {
    cacheDisabled = false;
});

var trailerService = {

    getTrailerUrls: function(resource, onSuccess, onError) {
        if (!validUrl.isUri(resource)) {
            throw 'Bad resource url'
        }
        var _this = this;

        this._getCached(resource, function(cachedOptional) {
            cachedOptional
                .map(onSuccess)
                .orElse(function() {
                    viaPlayApi.getImdbId(resource)
                        .then(function(data) {
                            trailerAddictApi
                                .getTrailers(data)
                                .then( _this._cacheAndExec(resource, onSuccess) )
                                .catch(onError)
                        })
                        .catch(onError);
                });
        });

    },

    _getCached: function(resource, cb) {
        if (cacheDisabled) {
            cb( new Optional() );
            return;
        }

        client.get(resource, function(err, data) {
            // Wrap return with optional (data will be null if no value for key
            cb( new Optional(data) );
        });
    },

    _cacheAndExec: function(key, afterSetCb) {
        return function(data) {
            if (cacheDisabled) {
                afterSetCb(data);
                return;
            }
            client.set(key, data, function() {
                afterSetCb(data);
            });
        }
    }
};

module.exports = trailerService;
