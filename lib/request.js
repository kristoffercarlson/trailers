var http    = require("http");
//var https   = require("https");
var url     = require('url');

/**
 * Trailer module (request methods)
 * @type {{}}
 */
var apiRequest = {
    request: function(params) {
        var endpoint = url.parse(params.url),
            options = {
                hostname           : endpoint.host,
                path               : endpoint.path,
                method             : params.method || 'GET',
                headers  : {
                    'Accept'            :'application/json'
                }
            },
            req = http.request(options, function(res) {
                var rawData = '';
                res.setEncoding('utf8');
                res.on('data', function(chunk) {
                    rawData += chunk;
                });
                res.on('end', function() {
                    console.log('------------- HTTPS REQUEST COMPLETE -------------');
                    console.log('Endpoint: ', params.url);
                    console.log(res.statusMessage);
                    console.log('--------------------------------------------------');
                    console.log(rawData);
                    console.log('--------------------------------------------------');
                    params.callback(null, rawData);
                });
            });

        req.on('error', function(e) {
            params.callback(e, null);
        });

        if (params.body) {
            req.write(params.body);
        }

        req.end();
    }
};

module.exports = apiRequest;
