/**
 * Basic optionals class
 * @param arg
 * @constructor
 */
var Optional = function(arg) {
    this.value = (typeof arg === 'function') ? arg() : arg;
};

Optional.prototype.map = function(fn) {
    if (this.value) fn(this.value);
    return this;
};

Optional.prototype.orElse = function(fn) {
    if (!this.value) fn();
};

module.exports = Optional;